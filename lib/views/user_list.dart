import 'package:flutter/material.dart';
import 'package:leonardo_leitao_firebase/components/user_tile.dart';
import 'package:leonardo_leitao_firebase/models/user.dart';
import 'package:leonardo_leitao_firebase/provider/users.dart';
import 'package:leonardo_leitao_firebase/routes/app_routes.dart';
import 'package:provider/provider.dart';

class UserList extends StatelessWidget {
  const UserList({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    final Users users = Provider.of(context);

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Lista De Usuários"),
        actions: [
          IconButton(icon: Icon(Icons.add),
              onPressed: (){
               Navigator.of(context).pushNamed(
                 AppRoutes.USER_FORM
               );
              }
              )
        ],
      ),
      body: ListView.builder(
        itemCount: users.count,
          itemBuilder: (BuildContext context, int index){
          return UserTile(users.byIndex(index));
          }
      ),
    );
  }
}
