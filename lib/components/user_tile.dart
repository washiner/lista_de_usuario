import 'package:flutter/material.dart';
import 'package:leonardo_leitao_firebase/models/user.dart';
import 'package:leonardo_leitao_firebase/provider/users.dart';
import 'package:leonardo_leitao_firebase/routes/app_routes.dart';
import 'package:provider/provider.dart';

class UserTile extends StatelessWidget {
  final User user;
  const UserTile(this.user);

  @override
  Widget build(BuildContext context) {
    final avatar = user.avatarUrl == null || user.avatarUrl.isEmpty
        ? CircleAvatar(child: Icon(Icons.person))
        : CircleAvatar(backgroundImage: NetworkImage(user.avatarUrl));
    return ListTile(
      leading: avatar,
      title: Text(user.name),
      subtitle: Text(user.email),
      trailing: Container(
        width: 100,
        child: Row(
          children: [
            IconButton(
                icon: Icon(Icons.edit),
                color: Colors.orange,
                onPressed: () {
                  Navigator.of(context).pushNamed(
                    AppRoutes.USER_FORM,
                    arguments: user,
                  );
                }),
            IconButton(
                icon: Icon(Icons.delete),
                color: Colors.red,
                onPressed: () {
                  showDialog(
                    context: context,
                    builder: (_) => AlertDialog(
                      title: Text("Excluir Usuário"),
                      content: Text("Tem Certeza??>"),
                      actions: [
                        TextButton(
                          child: Text("Não"),
                          onPressed: () => Navigator.of(context).pop(false),
                        ),
                        TextButton(
                          child: Text("Sim"),
                          onPressed: () {
                            //desse jeito nao precisa do pop true ou false e so fazer o them ali em baixo
                            //mas pra ficar organizado e seguir a aula do leonardo ele fez assim
                            // Provider.of<Users>(context, listen: false)
                            //     .remove(user);
                            Navigator.of(context).pop(true);
                          },
                        ),
                      ],
                    ),
                  ).then((confirmed) {
                    if (confirmed) {
                      Provider.of<Users>(context, listen: false).remove(user);
                    }
                  });
                }),
          ],
        ),
      ),
    );
  }
}
