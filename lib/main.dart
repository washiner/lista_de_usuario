import 'package:flutter/material.dart';
import 'package:leonardo_leitao_firebase/provider/users.dart';
import 'package:leonardo_leitao_firebase/routes/app_routes.dart';
import 'package:leonardo_leitao_firebase/views/user_form.dart';
import 'package:leonardo_leitao_firebase/views/user_list.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
        create: (context) => Users(),
        )
      ],
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Flutter Demo',
          theme: ThemeData(
            primarySwatch: Colors.blue,
          ),
            routes: {
            AppRoutes.HOME: (_) => UserList(),
            AppRoutes.USER_FORM: (_) => UserForm()
        },
        ),
    );
  }
}
