import 'package:leonardo_leitao_firebase/models/user.dart';

const DAMMU_USERS = {
  "1": const User(
    id: "1",
    name: "Maria",
    email: "maria@gmail.com",
    avatarUrl: "https://cdn.pixabay.com/photo/2021/05/02/07/56/child-6222810_960_720.png"
  ),
  "2": const User(
      id: "2",
      name: "Rafael",
      email: "Rafael@gmail.com",
      avatarUrl: "https://cdn.pixabay.com/photo/2017/01/31/21/23/avatar-2027365_960_720.png"
  ),
  "3": const User(
      id: "3",
      name: "Joaquina",
      email: "joaquina@gmail.com",
      avatarUrl: "https://cdn.pixabay.com/photo/2021/01/31/08/58/woman-5966300_960_720.png"
  ),
  "4": const User(
      id: "4",
      name: "Lucas",
      email: "lucas@gmail.com",
      avatarUrl: "https://cdn.pixabay.com/photo/2016/12/07/21/01/cartoon-1890438_960_720.jpg")
};